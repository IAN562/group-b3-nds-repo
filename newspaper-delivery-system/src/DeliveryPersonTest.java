import junit.framework.TestCase;

public class DeliveryPersonTest extends TestCase {

	// Test no. 1
	// Objective to test: To test Name for valid chars
	// Input(s): UserName = "Tommy12345"
	// Expected Output: False
	public void testvalidateName001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validateName("Tommy12345"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 2
	// Objective to test: To test Name valid chars
	// Input(s): UserName = "Tommy"
	// Expected Output: True
	public void testvalidateName002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validateName("Tommy"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 3
	// Objective to test: To test username for valid chars
	// Input(s): UserName = "Tommy12345"
	// Expected Output: True
	public void testvalidateUsername003() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validateUsername("Tommy12345"));

		} catch (Exception e) {
			fail("Exception not Expected");
		}

	}

	// Test no. 4
	// Objective to test: To test username valid chars
	// Input(s): UserName = "Tommy"
	// Expected Output: False
	public void testvalidateUsername004() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validateUsername("!%$&"));

		} catch (Exception e) {
			fail("Exception expected");
		}

	}

	// Test no. 5
	// Objective to test: To test password in range 0 to 3 chars
	// Input(s): Password = "abc"
	// Expected Output: False
	public void testvalidatePassword005() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validatePassword("abc"));

		} catch (Exception e) {
			fail("Exception expected");
		}
	}

	// Test no. 6
	// Objective to test: To test password in range 100 to MAX chars
	// Input(s): Password = "abcdefghijklmnoprstuywvz"
	// Expected Output: False
	public void testvalidatePassword006() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validatePassword("abcdefghijklmnoprstuywvz"));

		} catch (Exception e) {
			fail("Exception expected");
		}
	}

	// Test no. 7
	// Objective to test: To test password in range 4 to 9 chars
	// Input(s): Password = "abcdefgh"
	// Expected Output: True
	public void testvalidatePassword007() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validatePassword("abcdefgh"));

		} catch (Exception e) {
			fail("Exception not expected");
		}
	}

	// Test no. 8
	// Objective to test: To test phone number invalid chars
	// Input(s): Password = "abcdefghijklmnoprstuywvz"
	// Expected Output: False
	public void testvalidatePhoneNumber008() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validatePhoneNumber("abcdefghijklm"));

		} catch (Exception e) {
			fail("Exception expected");
		}
	}

	// Test no. 9
	// Objective to test: To test phone number valid chars
	// Input(s): Password = "abcdefgh"
	// Expected Output: True
	public void testvalidatePhoneNumber009() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validatePhoneNumber("0891234567"));

		} catch (Exception e) {
			fail("Exception not expected");
		}
	}

	// Test no. 10
	// Objective to test: If the Region is not empty
	// Input(s): RegionID = "15"
	// Expected Output: true
	public void testvalidateAssignedRegion010() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(true, testObject.validateAssignedRegion("ATH 1, ATH 2"));

		} catch (Exception e) {
			fail("Exception Expected");
		}
	}

	// Test no. 11
	// Objective to test: If the Region is empty
	// Input(s): RegionID = "0"
	// Expected Output: false
	public void testvalidateAssignedRegion011() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals(false, testObject.validateAssignedRegion(""));

		} catch (Exception e) {
			fail("Exception not Expected");
		}
	}
	
	public void testvalidatecreateDeliveryPerson012() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("ok", testObject.createDeliveryPerson("John", "John1234", "12qwert4%", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson013() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Name", testObject.createDeliveryPerson("123�John$", "John1234", "12qwert4%", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson014() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Username", testObject.createDeliveryPerson("John", "$$$$t%%%%", "12qwert4%", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson015() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Password", testObject.createDeliveryPerson("John", "John1234", "abc", "0891234567","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson016() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Phone Number", testObject.createDeliveryPerson("John", "John1234", "12qwert4%", "testPhone","Address","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson017() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Address", testObject.createDeliveryPerson("John", "John1234", "12qwert4%", "0891234567","","ATH 1, ATH 2"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateDeliveryPerson018() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			assertEquals("Incorrect Region(s)", testObject.createDeliveryPerson("John", "John1234", "12qwert4%", "0891234567","Address",""));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	// Test no. 19
	// Objective to test: To test valid value deliveryPerson
	// Input(s): deliveryPerson = "abc"
	// Expected Output: new DeliveryPerson()
	public void testviewDeliveryPersonDetails019() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.viewDeliveryPersonDetails("abc") instanceof DeliveryPerson);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	// Test no. 20
	// Objective to test: To test invalid value deliveryPerson
	// Input(s): deliveryPerson = ""
	// Expected Output: null
	public void testviewDeliveryPersonDetails020() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			DeliveryPerson testObject = new DeliveryPerson(db);
			
			assertEquals(true, testObject.viewDeliveryPersonDetails("") == null);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
}
