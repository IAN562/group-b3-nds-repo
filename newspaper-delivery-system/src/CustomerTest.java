import java.sql.ResultSet;

import junit.framework.TestCase;

public class CustomerTest extends TestCase {

	// Test no. 1
	// Objective to test: To test LN int vs str compare
	// Input(s): UserName = "abc"
	// Expected Output: False
	public void testvalidateLastName001() {
		
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateLastName("Donoghue12345"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 2
	// Objective to test: To test LN Letters compare
	// Input(s): UserName = "Donoghue"
	// Expected Output: True
	public void testvalidateLastName002() {
	
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateLastName("Donoghue"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 3
	// Objective to test: To test LN int vs str compare
	// Input(s): UserName = "abc"
	// Expected Output: False
	public void testvalidateOtherNames003() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateOtherNames("Donoghue12345"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 4
	// Objective to test: To test LN Letters compare
	// Input(s): UserName = "Donoghue"
	// Expected Output: True
	public void testvalidateOtherNames004() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateOtherNames("Donoghue"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 5
	// Date of birth tests
	public void testvalidateDOB005() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateDOB("11-22-96"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 6
	public void testvalidateDOB006() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateDOB("112-22-9a"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 7
	// Address tests
	public void testvalidateAddress007() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateAddress("# Wilow Park 123"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 8
	public void testvalidateAddress008() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateAddress("Willow Park 123"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Region Id
	// Test no. 9
	public void testvalidateRegionId009() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateRegion("1"));
		} catch (Exception e) {
			fail("Exception not Expected");
		}

	}

	// Test no. 10
	public void testvalidateRegionId010() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateRegion(""));
		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 11
	// Objective to test: To test LN int vs str compare
	// Input(s): UserName = "abc"
	// Expected Output: False
	public void testvalidateDeliveryFrequency011() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateDeliveryFrequency("0"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 12
	// Objective to test: To test LN Letters compare
	// Input(s): UserName = "Donoghue"
	// Expected Output: True
	public void testvalidateDeliveryFrequency012() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateDeliveryFrequency("1"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 13
	public void testvalidatephoneNumber013() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validatePhoneNumber("dfcghi"));

		} catch (Exception e) {
			fail("Exception expected");
		}

	}

	// Test no. 14
	public void testvalidatephoneNumber014() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validatePhoneNumber("0987654321"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	
	// Test no. 15
	public void testvalidatecreateCustomer015() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Last Name", testObject.createCustomer("123%$Fallon", "John", "22-12-18", "83 Auburn Heights, Athlone", "ATH 1, ATH 2", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}
	
	// Test no. 16
	public void testvalidatecreateCustomer016() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Other Name", testObject.createCustomer("Fallon", "123%John$%", "22-12-18", "83 Auburn Heights, Athlone", "ATH 1, ATH 2", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}
	}
	
	// Test no. 17
	public void testvalidatecreateCustomer017() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect DOB", testObject.createCustomer("Fallon", "John", "testDOB", "83 Auburn Heights, Athlone", "ATH 1, ATH 2", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
	
	// Test no. 18
	public void testvalidatecreateCustomer018() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Address", testObject.createCustomer("Fallon", "John", "22-12-18", "/83 A%ub%urn$ Heig^hts, Athlone", "ATH 1, ATH 2", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
	
	// Test no. 19
	public void testvalidatecreateCustomer019() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Region", testObject.createCustomer("Fallon", "John", "22-12-18", "83 Auburn Heights, Athlone", "", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
	// Test no. 20
	public void testvalidatecreateCustomer020() {

		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Delivery Freq", testObject.createCustomer("Fallon", "John", "22-12-18", "83 Auburn Heights, Athlone", "ATH 1", "0", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	// Test no. 21
	public void testvalidatecreateCustomer021() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Phone Number", testObject.createCustomer("Fallon", "John", "22-12-18", "83 Auburn Heights, Athlone", "ATH 1S", "Weekly", "testblabla"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}

	// Test no. 22
	// Objective to test: To test valid boundary value for customerId (Integer.MAX_VALUE >= customerId > 0)
	// Input(s): customerId = 1
	// Expected Output: true
	public void testvalidateCustomerId022() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			
			assertEquals(true, testObject.validateCustomerId(1));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 23
	// Objective to test: To test invalid boundary values for customerId (customerId <= 0)
	// Input(s): customerId = 0
	// Expected Output: false
	public void testvalidateCustomerId023() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			
			assertEquals(false, testObject.validateCustomerId(0));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 24
	// Objective to test: To test invalid boundary values for customerId (customerId > Integer.MAX_VALUE)
	// Input(s): customerId = Integer.MAX_VALUE + 1
	// Expected Output: false
	public void testvalidateCustomerId024() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			
			assertEquals(false, testObject.validateCustomerId(Integer.MAX_VALUE + 1));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 25
	// Objective to test: To test valid boundary values for publicationIds (Integer.MAX_VALUE >= publicationIds > 0)
	// Input(s): publicationIds = {1, 2, 3, 4, 5, 100}
	// Expected Output: true
	public void testvalidatePublicationIds025() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			int[] publicationIds = { 1, 2, 3, 4, 5, 100 };
			
			assertEquals(true, testObject.validatePublicationIds(publicationIds));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 26
	// Objective to test: To test invalid boundary values for publicationIds (Integer.MAX_VALUE < publicationIds)
	// Input(s): publicationIds = {1, 2, Integer.MAX_VALUE + 1, 4, 5, 100}
	// Expected Output: false
	public void testvalidatePublicationIds026() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			int[] publicationIds = {1, 2, Integer.MAX_VALUE + 1, 4, 5, 100};

			assertEquals(false, testObject.validatePublicationIds(publicationIds));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 27
	// Objective to test: To test invalid boundary values for publicationIds (publicationIds <=0)
	// Input(s): publicationIds = {1, 2, 3, 0, 5, 100}
	// Expected Output: false
	public void testvalidatePublicationIds027() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			int[] publicationIds= {1, 2, 3, 0, 5, 100};
			
			assertEquals(false, testObject.validatePublicationIds(publicationIds));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 28
	// Objective to test: To test invalid values for customerId and publicationIds
	// Input(s): publicationIds = {1, 2, 3, 0, 5, 100}, customer = 0
	// Expected Output: "Select a customer and the required publication(s)."
	public void testeditCustomerSubscriptions028() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			int[] publicationIds = {1, 2, 3, 0, 5, 100};
			
			assertEquals("Select a customer and the required publication(s).", testObject.editCustomerSubscriptions(0, publicationIds));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 29
	// Objective to test: To test valid value for customerId and invalid values publicationIds
	// Input(s): customer = 1, publicationIds = {1, 2, 3, 0, 5, 100}
	// Expected Output:  "Select the required publication(s)."
	public void testeditCustomerSubscriptions029() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			int[] publicationIds = {1, 2, 3, 0, 5, 100};
			
			assertEquals( "Select the required publication(s).", testObject.editCustomerSubscriptions(1, publicationIds));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 30
	// Objective to test: To test invalid value for customerId and valid values publicationIds
	// Input(s): customer = 0, publicationIds = {1, 2, 3, 4, 5, 100}
	// Expected Output: "Select a customer."
	public void testeditCustomerSubscriptions030() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			int[] publicationIds = {1, 2, 3, 4, 5, 100};
			
			assertEquals("Select a customer.", testObject.editCustomerSubscriptions(0, publicationIds));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 31
	// Objective to test: To test valid value for customerId and valid values publicationIds
	// Input(s): customer = 1, publicationIds = {1, 2, 3, 4, 5, 100}
	// Expected Output: "Customer has been successfully subscribed to the selected publication(s)."
	public void testeditCustomerSubscriptions031() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			int[] publicationIds = {1, 2, 3, 4, 5, 100};
			
			assertEquals("Customer has been successfully subscribed to the selected publication(s).", testObject.editCustomerSubscriptions(1, publicationIds));
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 32
	// Objective to test: To test valid value customerName
	// Input(s): customerName = "abc"
	// Expected Output: new Customer()
	public void testeditCustomerSubscriptions032() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			
			assertEquals(true, testObject.viewCustomerDetails("abc") instanceof ResultSet);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
	
	// Test no. 33
	// Objective to test: To test invalid value customerName
	// Input(s): customerName = ""
	// Expected Output: null
	public void testeditCustomerSubscriptions0343() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			
			assertEquals(true, testObject.viewCustomerDetails("") == null);
		} catch(Exception ex) {
			fail("Exception not expected");
			ex.printStackTrace();
		}
	}
}
