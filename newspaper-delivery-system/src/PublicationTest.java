import junit.framework.TestCase;

public class PublicationTest extends TestCase {

	// Test no. 1
	// Objective to test: To test PublicationName for invalid chars
	// Input(s): Pub Name = "~#Irish Times"
	// Expected Output: False
	public void testvalidatePublicationName001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationName("~#IrishTimes"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 2
	// Objective to test: To test PublicationName for valid chars
	// Input(s): Pub Name = "Irish Times"
	// Expected Output: True
	public void testvalidatePublicationName002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationName("IrishTimes"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 3
	// Objective to test: To test PublicationDate for valid chars
	// Input(s): Pub Date = "11-22-96"
	// Expected Output: True

	public void testvalidatePublicationDate001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationDate("11-22-96"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 4
	// Objective to test: To test PublicationDate for valid chars
	// Input(s): Pub Date = "112-22-9a"
	// Expected Output: False

	public void testvalidatePublicationDate002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationDate("112-22-9a"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 5
	// Objective to test: To test publication cost for invalid number
	// Input(s): Pub Cost = "-2"
	// Expected Output: False

	public void testvalidatePublicationCost001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationCost(-2));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 6
	// Objective to test: To test publication cost for valid number
	// Input(s): Pub Cost = "22"
	// Expected Output: True

	public void testvalidatePublicationCost002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationCost(22));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 7
	// Objective to test: To test invalid publication frequency
	// Input(s): Pub Cost = ""
	// Expected Output: False

	public void testvalidatePublicationFrequency001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationFrequency(""));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 8
	// Objective to test: To test valid publication frequency
	// Input(s): Pub Cost = "Weekly"
	// Expected Output: True

	public void testvalidatePublicationFrequency002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationFrequency("Weekly"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 9
	// Objective to test: To test PublicationName for invalid chars
	// Input(s): Pub genere = "#$%Spor%t"
	// Expected Output: False
	public void testvalidatePublicationGenere001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(false, testObject.validatePublicationGenere("#$%Spor%t"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 10
	// Objective to test: To test PublicationName for invalid chars
	// Input(s): Pub genere = "Sport"
	// Expected Output: True
	public void testvalidatePublicationGenere002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals(true, testObject.validatePublicationGenere("Sport"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	public void testvalidatecreatePublication001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("ok", testObject.createPublication("Irish Times", "22-12-18", 5.5, "Weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}

	public void testvalidatecreatePublication002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Name",
					testObject.createPublication("#Iri~sh Times", "22-12-18", 5.5, "Weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}

	public void testvalidatecreatePublication003() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Date",
					testObject.createPublication("Irish Times", "222-12-1a", 5.5, "Weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}

	public void testvalidatecreatePublication004() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Cost",
					testObject.createPublication("Irish Times", "22-12-18", 0, "Weekly", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}

	public void testvalidatecreatePublication005() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObject = new Publication(db);
			assertEquals("Incorrect Publication Frequency",
					testObject.createPublication("Irish Times", "22-12-18", 5.5, "", "Sport"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}
	}

	public void testvalidatecreatePublication006() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Publication testObj = new Publication(db);
			assertEquals("Incorrect Publication Genere",
					testObj.createPublication("Irish Times", "22-12-18", 5.5, "Weekly", "S#po~rt"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}

}
