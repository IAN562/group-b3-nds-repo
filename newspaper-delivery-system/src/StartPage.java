import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StartPage {

	private JFrame frame;
	DatabaseConnector dbc;

	/**
	 * Launch the application.
	 */
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartPage window = new StartPage(null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void StartScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartPage window = new StartPage(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StartPage(DatabaseConnector dbobj) throws Exception {
		dbc = dbobj;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Start Page");
		frame.setBounds(100, 100, 414, 242);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblWelcome = new JLabel("Welcome");
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblWelcome.setBounds(140, 11, 96, 38);
		frame.getContentPane().add(lblWelcome);
		
		JLabel lblToNewsPaper = new JLabel("To News Paper Delivery System");
		lblToNewsPaper.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblToNewsPaper.setBounds(84, 42, 227, 38);
		frame.getContentPane().add(lblToNewsPaper);
		
		Button button = new Button("Login Newsagent");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					NewsagentLogin nw =new NewsagentLogin(dbc);
					nw.NewsagentLoginScreen(dbc);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
				
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button.setFont(new Font("Dialog", Font.BOLD, 12));
		button.setBounds(27, 117, 143, 38);
		frame.getContentPane().add(button);
		
		Button button_1 = new Button("Login Delivery Person");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					DeliveryPersonLogin nw =new DeliveryPersonLogin(dbc);
					nw.DeliveryPersonLoginScreen(dbc);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
				
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
				
			}
		});
		button_1.setFont(new Font("Dialog", Font.BOLD, 12));
		button_1.setBounds(225, 117, 143, 38);
		frame.getContentPane().add(button_1);
	}

}
