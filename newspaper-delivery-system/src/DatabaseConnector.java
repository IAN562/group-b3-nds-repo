import java.sql.Statement;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.CallableStatement;
import java.sql.Types;

public class DatabaseConnector {
	String cmd = null;
	Connection con = null;
	Statement stmt = null;
	CallableStatement cStmt = null;
	ResultSet rs = null;

	public DatabaseConnector() throws Exception {
		initiate_db_conn();
	}

	public void initiate_db_conn() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		String url="jdbc:mysql://localhost:3306/newspaper_delivery_system?useSSL=no";

		con = DriverManager.getConnection(url, "<change username>", "<change password>");
		stmt = con.createStatement();
	}

	public boolean createCustomerInDatabase(
			String lastName,
			String otherNames,
			String dateOfBirth,
			String phoneNumber,
			String address,
			String region,
			String deliveryFrequency
			) throws Exception {
		String sqlStr = "";
		int result = 0;

		if (lastName != "" && otherNames != "" && dateOfBirth != "" && phoneNumber != "" && address != "" && region != "" && (deliveryFrequency == "daily" || deliveryFrequency == "weekly" || deliveryFrequency == "monthly")) {
			sqlStr = "" +
					"insert into customers (" +
					"  last_name," + 
					"  other_names," + 
					"  date_of_birth," + 
					"  phone_number," + 
					"  address," + 
					"  region," + 
					"  delivery_frequency," +
					"  subscription_id" +
					") values (" + 
					"  '" + lastName + "'," +
					"  '" + otherNames + "'," +
					"  '" + dateOfBirth + "'," +
					"  '" + phoneNumber + "'," +
					"  '" + address + "'," +
					"  '" + region + "'," +
					"  '" + deliveryFrequency + "'," +
					"  '0'" +
					");"
					;

			result = stmt.executeUpdate(sqlStr);

			if (result >= 1) return true;
			else return false;
		} else {
			return false;
		}
	}

	public boolean createRegionInDatabase(String regionName) throws Exception {
		String sqlStr = "";
		int result = 0;

		if (regionName != "") {
			sqlStr = "" +
					"insert into regions (" + 
					"  region_name" +
					") values (" + 
					"  '" + regionName + "'" +
					");"
					;

			result = stmt.executeUpdate(sqlStr);

			if (result >= 1) return true;
			else return false;
		} else {
			return false;
		}
	}

	//	public boolean createDeliveryPersonInDatabase_old(
	//			String name,
	//			String username,
	//			String password,
	//			String phoneNumber,
	//			String address,
	//			String assignedRegionIds
	//	) throws Exception {
	//		String sqlStr = "";
	//		int result = 0;
	//
	//		if (name != "" && username != "" && password != "" && phoneNumber != "" && address != "" && assignedRegionIds != "") {
	//			sqlStr = "{ call create_delivery_person(?, ?, ?, ?, ?, ?, ?) }";
	//			cStmt = con.prepareCall(sqlStr);
	//			cStmt.setString("name_param", name);
	//			cStmt.setString("username_param", username);
	//			cStmt.setString("password_param", password);
	//			cStmt.setString("phoneNumber_param", phoneNumber);
	//			cStmt.setString("address_param", address);
	//			cStmt.setString("assignedRegionIds_param", assignedRegionIds);
	//			cStmt.registerOutParameter("update_count", Types.INTEGER);
	//			cStmt.execute();
	//			result = cStmt.getInt("update_count");
	//			
	//			if (result >= 1) 
	//				return true
	//			; else 
	//				return false
	//			;
	//			
	//		} else {
	//			return false;
	//		}
	//	}
	//	
	public boolean createDeliveryPersonInDatabase(
			String name,
			String username,
			String password,
			String phoneNumber,
			String address,
			String assignedRegions
			) throws Exception {
		String sqlStr = "";
		int result = 0;

		if (name != "" && username != "" && password != "" && phoneNumber != "" && address != "" && assignedRegions != "") {
			sqlStr = "" +
					"insert into delivery_persons (" + 
					"		name," + 
					"        username," + 
					"        password," + 
					"        phone_number," + 
					"        address," + 
					"        assigned_regions" + 
					"    ) values (" + 
					"		'"+ name + "'," + 
					"        '"+ username + "'," + 
					"        '"+ password + "'," + 
					"        '"+ phoneNumber + "'," + 
					"        '"+ address + "'," + 
					"        '"+ assignedRegions + "'" +
					"    );"
					;

			result = stmt.executeUpdate(sqlStr);

			if (result >= 1) 
				return true
						; else 
							return false
									;

		} else {
			return false;
		}
	}

	//	public boolean createGenreInDatabase(String genreName) throws Exception {
	//		String sqlStr = "";
	//		int result = 0;
	//		
	//		if (genreName != "") {
	//			sqlStr = "" +
	//					"insert into genres (" + 
	//					"  genre_name" +
	//					") values (" + 
	//					"  '" + genreName + "'" +
	//					");"
	//			;
	//			
	//			result = stmt.executeUpdate(sqlStr);
	//			
	//			if (result >= 1) return true;
	//			else return false;
	//		} else {
	//			return false;
	//		}
	//	}



	/////////////////////////////////////////////////////////////////////////////////////
	
	public boolean updateCustomerSubscriptions(String subscription, int id) throws Exception {
		String sqlStr = "";
		int result = 0;
		

		if (subscription != "") {
			sqlStr = "update customers set subscription_id ='"+subscription+"' where id = '"+id+"'";


			result = stmt.executeUpdate(sqlStr);

			if (result == 1) return true;
			else return false;
		} else {
			return false;
		}
	}



	public ResultSet retrieveCustomerName() throws Exception {
		String sqlStr = "SELECT id, last_name, other_names FROM customers";
		//String result = "";
		ResultSet rs = stmt.executeQuery(sqlStr);

		if (!rs.equals(null)) {
			return rs;
		} else { 
			return null; 
		}
		
	}
	
	
	
	public ResultSet retrievePublicationName() throws Exception {
		String sqlStr = "SELECT id, publication_name FROM publications";
		//String result = "";
		ResultSet rs = stmt.executeQuery(sqlStr);

		if (!rs.equals(null)) {
			return rs;
		} else { 
			return null; 
		}
		
	}


	public String retrieveCustomerSubscriptions(int id) throws Exception {
		String sqlStr = "SELECT * FROM customers";
		String result = "";
		String subscription = "";
		ResultSet rs = stmt.executeQuery(sqlStr);

		while (rs.next())   //changes cursor to next row
		{

			if(id == rs.getInt("id") ){
				subscription = rs.getString("subscription_id");			
				if(subscription.equals("0")) {result="0";break;}
				else
				result = convertSubscriptions(subscription);
				break;
			}
			
			
		}
		System.out.println(result);
		return result;
	}

	
	public boolean checkNewsAgentsLogin(String name,String password) throws Exception { // check newsagents login check
		String sqlStr = "SELECT * FROM newsagent_login";
		ResultSet rs = stmt.executeQuery(sqlStr);
		boolean result = false;
		
		while (rs.next()) {
			String username = rs.getString("n_username");
			String pass = rs.getString("n_password");
			
			if (username.equals(name)&& pass.equals(password)) {
				result = true;
			}
		}
		
		return result; 
			
		
	}
	
	
	public boolean checkDeliveryDriverLogin(String name,String password) throws Exception { // delivery person login check
		String sqlStr = "SELECT * FROM delivery_persons";
		ResultSet rs = stmt.executeQuery(sqlStr);
		boolean result = false;
		
		while (rs.next()) {
			String username = rs.getString("username");
			String pass = rs.getString("password");
			
			if (username.equals(name)&& pass.equals(password)) {
				result = true;
			}
		}
		
		return result; 
			
	}
	
	
	
	public ResultSet retrieveDeliveryDriverDetails(String name) throws Exception {
		String sqlStr = "SELECT id, name, username , phone_number, address, assigned_regions FROM delivery_persons";
		
		if (!name.equals("")) {
			sqlStr += " WHERE username = '" + name + "'";
		}
		
		ResultSet rs = stmt.executeQuery(sqlStr);

		if (!rs.equals(null)) {
			return rs;
		} else { 
			return null; 
		}
		
		
		
	}
	
	

	public ResultSet retrieveCustomerDetails(String name) throws Exception {
		String sqlStr = "select * from customers where concat(last_name, ' ', other_names)like '%"+name+"%';";
		ResultSet rs = stmt.executeQuery(sqlStr);

		if (!rs.equals(null)) {
			return rs;
		} else { 
			return null; 
		}
		
	}
	
	
	
	public String convertSubscriptions(String Publication_ids) throws Exception {
		String sqlStr2 = "SELECT * FROM publications";
		String result = "";
		ResultSet rs2 = stmt.executeQuery(sqlStr2);
		
		String[] parts = Publication_ids.split(",");
		

		while (rs2.next())   //changes cursor to next row
		{
			int idvalue = rs2.getInt("id");
			String id = (String.valueOf(idvalue));
			
			for (int i=0;i<parts.length;i++){
				
				if(id.equals(parts[i])) {
				
			
			String name =rs2.getString("publication_name");
			result += name+",";
				}
			
			}

		}
		System.out.println(result);
		return result;
		
	}



	/////////////////////////////////////////////////////////////////////////////////////////////////////////


	public boolean createPublicationInDatabase(
			String publicationName,
			String publicationDate,
			double cost,
			String publicationFrequency,
			String genre
			) throws Exception {
		String sqlStr = "";
		int result = 0;

		if (publicationName != "" && publicationDate != "" && cost > 0 && cost <= 1000 && (publicationFrequency == "daily" || publicationFrequency == "weekly" || publicationFrequency == "monthly") && genre != "") {
			sqlStr = "" +
					"insert into publications (" +
					"  publication_name," + 
					"  publication_date," + 
					"  cost," + 
					"  publication_frequency," + 
					"  genre" + 
					") values (" + 
					"  '" + publicationName + "'," +
					"  '" + publicationDate + "'," +
					"  '" + cost + "'," +
					"  '" + publicationFrequency + "'," +
					"  '" + genre + "'" +
					");"
					;

			result = stmt.executeUpdate(sqlStr);

			if (result >= 1) return true;
			else return false;
		} else {
			return false;
		}
	}


	//	public boolean updateCustomerSubscription(
	//			String lastName,
	//			String otherNames,
	//			String subscription_id,
	//			
	//	) throws Exception {
	//		String sqlStr = "";
	//		int result = 0;
	//		
	//		if (lastName != "" && otherNames != "" && subscription_id != "")) {
	//			sqlStr = "" +
	//					"insert into customers (" +
	//					"  subscription_id," +
	//					") values (" + 
	//					"  '" + subscription_id +
	//					");"
	//			;
	//			
	//			result = stmt.executeUpdate(sqlStr);
	//			
	//			if (result >= 1) return true;
	//			else return false;
	//		} else {
	//			return false;
	//		}
	//	}
}


