import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.List;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.Arrays;
import java.awt.event.ActionEvent;

public class CustomerSubscriptions {

	private JFrame frame;
	String[] customerArr;
	int[] customerIdArr;
	String[] publicationArr;
	int[] publicationIdArr;
	JComboBox comboBox;
	JComboBox comboBox_1;
	JComboBox comboBox_2;
	JComboBox comboBox_3;
	JComboBox comboBox_4;
	JComboBox comboBox_5;
	JComboBox comboBox_6;
	JComboBox comboBox_7;
	JComboBox comboBox_8;
	JComboBox comboBox_9;
	JComboBox comboBox_10;
	private Customer customer;

	/**
	 * Launch the application.
	 */
	public static void AddSubScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerSubscriptions window = new CustomerSubscriptions(dbobj);
					window.frame.setVisible(true);
					window.loadComboBox(dbobj);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CustomerSubscriptions(DatabaseConnector dbobj) {
		initialize(dbobj);
	}

	public void loadComboBox(DatabaseConnector dbobj) throws Exception {
		int i = 0;
		int j = 0;
		ResultSet rs = dbobj.retrievePublicationName();

		rs.last();
		publicationIdArr = new int[rs.getRow()];
		publicationArr = new String[rs.getRow() + 1];
		publicationArr[0] = "Select a publication";
		rs.beforeFirst();
		while (rs.next()) {
			publicationIdArr[i] = rs.getInt("id");
			publicationArr[i + 1] = rs.getString("publication_name");
			i++;
		}
		
		rs = dbobj.retrieveCustomerName();
		rs.last();
		
		customerIdArr = new int[rs.getRow()];
		customerArr = new String[rs.getRow() + 1];
		customerArr[0] = "Select a customer";
		
		
		rs.beforeFirst();

		

		while (rs.next()) {
			customerIdArr[j] = rs.getInt("id");
			customerArr[j + 1] = rs.getString("other_names") + " " + rs.getString("last_name");
			j++;
		}

		comboBox.setModel(new DefaultComboBoxModel(customerArr));
		comboBox_1.setModel(new DefaultComboBoxModel(publicationArr));
		comboBox_2.setModel(new DefaultComboBoxModel(publicationArr));
		comboBox_3.setModel(new DefaultComboBoxModel(publicationArr));
		comboBox_4.setModel(new DefaultComboBoxModel(publicationArr));
		comboBox_5.setModel(new DefaultComboBoxModel(publicationArr));
		comboBox_6.setModel(new DefaultComboBoxModel(publicationArr));
		comboBox_7.setModel(new DefaultComboBoxModel(publicationArr));
		comboBox_8.setModel(new DefaultComboBoxModel(publicationArr));
		comboBox_9.setModel(new DefaultComboBoxModel(publicationArr));
		comboBox_10.setModel(new DefaultComboBoxModel(publicationArr));

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(DatabaseConnector dbobj) {
		customer = new Customer(dbobj);
		frame = new JFrame("Customer Subscriptions");
		frame.setBounds(100, 100, 549, 614);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblAddSubscription = new JLabel("Add Subscription");
		lblAddSubscription.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblAddSubscription.setBounds(147, 11, 179, 34);
		frame.getContentPane().add(lblAddSubscription);

		JLabel lblCustomer = new JLabel("Customer:");
		lblCustomer.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCustomer.setBounds(20, 63, 73, 23);
		frame.getContentPane().add(lblCustomer);

		comboBox = new JComboBox();
		comboBox.setBounds(123, 66, 199, 20);
		frame.getContentPane().add(comboBox);

		JLabel lblPublicationsSubscribed = new JLabel("1.");
		lblPublicationsSubscribed.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPublicationsSubscribed.setBounds(20, 152, 13, 23);
		frame.getContentPane().add(lblPublicationsSubscribed);

		comboBox_1 = new JComboBox();
		comboBox_1.setBounds(123, 155, 199, 20);
		frame.getContentPane().add(comboBox_1);

		JLabel lblNewPublication = new JLabel("2.");
		lblNewPublication.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewPublication.setBounds(20, 186, 13, 23);
		frame.getContentPane().add(lblNewPublication);

		comboBox_2 = new JComboBox();
		comboBox_2.setBounds(123, 251, 199, 20);
		frame.getContentPane().add(comboBox_2);

		JLabel lblNewPublications = new JLabel("Publications");
		lblNewPublications.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblNewPublications.setBounds(170, 118, 199, 23);
		frame.getContentPane().add(lblNewPublications);

		comboBox_3 = new JComboBox();
		comboBox_3.setBounds(123, 189, 199, 20);
		frame.getContentPane().add(comboBox_3);

		comboBox_4 = new JComboBox();
		comboBox_4.setBounds(123, 220, 199, 20);
		frame.getContentPane().add(comboBox_4);

		comboBox_5 = new JComboBox();
		comboBox_5.setBounds(123, 282, 199, 20);
		frame.getContentPane().add(comboBox_5);

		JLabel label = new JLabel("3.");
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label.setBounds(20, 220, 13, 23);
		frame.getContentPane().add(label);

		JLabel label_1 = new JLabel("4.");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_1.setBounds(20, 248, 13, 23);
		frame.getContentPane().add(label_1);

		JLabel label_2 = new JLabel("5.");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_2.setBounds(20, 279, 13, 23);
		frame.getContentPane().add(label_2);

		Button button = new Button("Save");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String pubids = "";
					int cusid = 0;
					String r;

					if (comboBox_1.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_1.getSelectedIndex() - 1];
					}

					if (comboBox_2.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_2.getSelectedIndex() - 1];
					}

					if (comboBox_3.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_3.getSelectedIndex() - 1];
					}

					if (comboBox_4.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_4.getSelectedIndex() - 1];
					}

					if (comboBox_5.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_5.getSelectedIndex() - 1];
					}

					if (comboBox_6.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_6.getSelectedIndex() - 1];
					}

					if (comboBox_7.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_7.getSelectedIndex() - 1];
					}

					if (comboBox_8.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_8.getSelectedIndex() - 1];
					}

					if (comboBox_9.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_9.getSelectedIndex() - 1];
					}

					if (comboBox_10.getSelectedIndex() != 0) {
						pubids += ", "
								+ publicationIdArr[comboBox_10.getSelectedIndex() - 1];
					}

					if (comboBox.getSelectedIndex() != 0) {
						cusid = customerIdArr[comboBox.getSelectedIndex() - 1];
					}

					pubids = pubids.substring(2);
					r = customer.editCustomerSubscriptions(cusid,
							Arrays.stream(pubids.split(",")).mapToInt(Integer::parseInt).toArray());

					JOptionPane.showConfirmDialog(frame, r, "Information", JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		button.setBounds(381, 517, 97, 34);
		frame.getContentPane().add(button);

		Button button_1 = new Button("Back");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					CustChoose nw = new CustChoose(dbobj);
					nw.NewScreen4(dbobj);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button_1.setBounds(55, 517, 97, 34);
		frame.getContentPane().add(button_1);

		Button button_2 = new Button("Add Publication");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		button_2.setBounds(385, 64, 104, 22);
		frame.getContentPane().add(button_2);

		comboBox_6 = new JComboBox();
		comboBox_6.setBounds(123, 313, 199, 20);
		frame.getContentPane().add(comboBox_6);

		comboBox_7 = new JComboBox();
		comboBox_7.setBounds(123, 344, 199, 20);
		frame.getContentPane().add(comboBox_7);

		comboBox_8 = new JComboBox();
		comboBox_8.setBounds(123, 375, 199, 20);
		frame.getContentPane().add(comboBox_8);

		comboBox_9 = new JComboBox();
		comboBox_9.setBounds(123, 406, 199, 20);
		frame.getContentPane().add(comboBox_9);

		comboBox_10 = new JComboBox();
		comboBox_10.setBounds(123, 437, 199, 20);
		frame.getContentPane().add(comboBox_10);

		JLabel label_3 = new JLabel("6.");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_3.setBounds(20, 310, 13, 23);
		frame.getContentPane().add(label_3);

		JLabel label_4 = new JLabel("7.");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_4.setBounds(20, 341, 13, 23);
		frame.getContentPane().add(label_4);

		JLabel label_5 = new JLabel("8.");
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_5.setBounds(20, 375, 13, 23);
		frame.getContentPane().add(label_5);

		JLabel label_6 = new JLabel("9.");
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_6.setBounds(20, 406, 13, 23);
		frame.getContentPane().add(label_6);

		JLabel label_7 = new JLabel("10.");
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_7.setBounds(20, 437, 21, 23);
		frame.getContentPane().add(label_7);

		
	}
}
